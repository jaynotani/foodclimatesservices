package com.foodclimates.controller;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.foodclimates.data.entity.Person;
import com.foodclimates.channelrepsonse.PersonChannelRepsonse;

@Controller
@RequestMapping("/person")
public class PersonController {

	@PersistenceContext
	private EntityManager entityManager;
	
	
	@Transactional
	@RequestMapping(value="/save", method=RequestMethod.POST)
	public @ResponseBody PersonChannelRepsonse savePersonRecord(@RequestBody Person person){
		
		PersonChannelRepsonse personChannelRepsonse = new PersonChannelRepsonse();
		
		personChannelRepsonse.setName(person.getName());
		personChannelRepsonse.setMobile_no(person.getMobile_no());
		
		
		Person personObj = new Person();
		personObj.setName(person.getName());
		personObj.setMobile_no(person.getMobile_no());
		
		personChannelRepsonse.setName(person.getName());
		personChannelRepsonse.setMobile_no(person.getMobile_no());
		
		entityManager.persist(personObj);
		
		return personChannelRepsonse;
		
	}
	
	@Transactional
	@RequestMapping(value="/test", method=RequestMethod.GET)
	public @ResponseBody String testPerson(@PathVariable String name){
		return name;
		
	}
}
