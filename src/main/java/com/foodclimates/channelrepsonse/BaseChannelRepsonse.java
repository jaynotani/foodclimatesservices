package com.foodclimates.channelrepsonse;

public class BaseChannelRepsonse {

	private String responseCode;
	private String errorCode;
	private String response;
	private String reponseStatus;
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public String getReponseStatus() {
		return reponseStatus;
	}
	public void setReponseStatus(String reponseStatus) {
		this.reponseStatus = reponseStatus;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
}
